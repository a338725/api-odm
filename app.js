const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
//const {expressjwt} = require('express-jwt');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const actorsRouter = require('./routes/actors');
const genresRouter = require('./routes/genres');
const directorsRouter = require('./routes/directors');
const moviesRouter = require('./routes/movies');
const membersRouter = require('./routes/members');
const bookingsRouter = require('./routes/bookings');
const copiesRouter = require('./routes/copies');
const awaitListRouter = require('./routes/await_lists');
// "mongodb://<dbUser>?:<dbPass>?@<direction>:<port>/<dbName>"

const uri= "mongodb://localhost:27017/videoClub";

mongoose.connect(uri);
const db = mongoose.connection;

db.on('open', ()=>{
  console.log('Conexion correcta');
});

db.on('error', ()=>{
  console.log('No se pudo conectar a la bd');
});

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const jwtKey = "123456789";
//app.use(expressjwt({secret:jwtKey,algorithms:['HS256']})
  //.unless({path:['/login']}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/actors',actorsRouter);
app.use('/genres',genresRouter);
app.use('/directors',directorsRouter);
app.use('/movies',moviesRouter);
app.use('/members',membersRouter);
app.use('/bookings',bookingsRouter);
app.use('/copies', copiesRouter);
app.use('/awaitlist', awaitListRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
