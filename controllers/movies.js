const express = require('express');
const Movie = require('../models/movie');
const Genre = require('../models/genre');


function list(req, res, next) {
    Movie.find().then(objs => res.status(200).json({
        message: "Lista de directores",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consutlar la informacion",
        obj:ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Movie.findOne({"_id":id}).then(obj => res.status(200).json({
        message:`Movie con id ${id}`,
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo encontrar un director con ID ${id}`,
        obj: ex
    }));
}


async function create(req, res, next) {
    const title = req.body.title;
    const genreId = req.body.genreId;
    const directorId = req.body.directorId;

    let genre = await Genre.findOne({"_id": genreId});

    let movie = new Movie({
        title: title,
        genre:genre
    });

    movie.save().then(obj => res.status(200).json({
        message:"Pelicula creada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo almacenar la pelicula",
        obj: ex
    })); 
}

function replace(req, res, next) {
    const id= req.params.id;
    let title = req.body.title ? req.body.title : "";
    let genreId = req.body.genreId ? req.body.genreId : false;

    let genre = new Object({
        _title:title,
        _genreId:genreId
    });

    Movie.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => res.status(200).json({
        message: "Objeto reemplazado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo reemplazar el director",
        obj: ex
    }));
}

function update(req, res, next) {
    const id= req.params.id;
    let title = req.body.title;
    let genreId = req.body.genreId;

    let genre = new Object();

    if(title){
        genre._title=title;
    }

    if(genreId){
        genre._genreId=genreId;
    }

    Movie.findOneAndUpdate({"_id":id}, genre,{new : true}).then(obj => res.status(200).json({
        message: "Objeto actualizado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo actualizar el director",
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Movie eliminado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo eliminar el director",
        obj: ex
    }));
}


module.exports = { list, index, create, replace, update, destroy };