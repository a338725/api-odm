
const express = require('express');
const Awaitlist = require('../models/await_list');
const Movie = require('../models/movie');
const Member = require('../models/member');

function list(req, res, next) {
    res.send('respond with list');
}

function index(req, res, next) {
    const id = req.params.id;
    res.send(`respond with index >>${id}`);
}


async function create(req, res, next) {
    const movieId = req.body.movieId;
    const memberId = req.body.memberId;

    let movie = await Movie.findOne({"_id": movieId});

    let member = await Member.findOne({"_id": memberId});

    let await_list = new Awaitlist({
        movie: movie,
        member: member
    });

    await_list.save().then(obj => res.status(200).json({
        message:"Se agrego a la cola",
        obj: obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo agregar a la cola",
        obj: ex
    }));
}

function replace(req, res, next) {
    res.send('respond with replace');
}

function update(req, res, next) {
    res.send('respond with update');
}

function destroy(req, res, next) {
    res.send('respond with destroy');
}


module.exports = { list, index, create, replace, update, destroy };