const express = require('express');
const Actor = require('../models/actor');


function list(req, res, next) {
    Actor.find().then(objs => res.status(200).json({
        message: "Lista de actorses",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consutlar la informacion",
        obj:ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Actor.findOne({"_id":id}).then(obj => res.status(200).json({
        message:`Actor con id ${id}`,
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo encontrar un actors con ID ${id}`,
        obj: ex
    }));
}


function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;

    let actors = new Actor({
        name: name,
        lastName:lastName
    });

    actors.save().then(obj => res.status(200).json({
        message:"Actor creado correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo almacenar el actors",
        obj: ex
    }));

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : false;

    let genre = new Object({
        _name:name,
        _lastName:lastName
    });

    Actor.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => res.status(200).json({
        message: "Objeto reemplazado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo reemplazar el actors",
        obj: ex
    }));
}

function update(req, res, next) {
    const id= req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let genre = new Object();

    if(name){
        genre._name=name;
    }

    if(lastName){
        genre._lastName=lastName;
    }

    Actor.findOneAndUpdate({"_id":id}, genre,{new : true}).then(obj => res.status(200).json({
        message: "Objeto actualizado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo actualizar el actors",
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Actor.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Actor eliminado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo eliminar el actors",
        obj: ex
    }));
}


module.exports = { list, index, create, replace, update, destroy };