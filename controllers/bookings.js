const express = require('express');
const Booking = require('../models/booking');
const Copy = require('../models/genre');
const Member = require('../models/genre');

function list(req, res, next) {
    Booking.find().then(objs => res.status(200).json({
        message: "Lista de directores",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consutlar la informacion",
        obj:ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Booking.findOne({"_id":id}).then(obj => res.status(200).json({
        message:`Booking con id ${id}`,
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo encontrar un director con ID ${id}`,
        obj: ex
    }));
}


async function create(req, res, next) {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copyId = req.body.copyId;

    let member = await Member.findOne({"_id": memberId});
    let copy = await Copy.findOne({"_id": copyId});

    let booking = new Booking({
        date: date,
        member:member,
        copy:copy
    });

    booking.save().then(obj => res.status(200).json({
        message:"Pelicula creada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo almacenar la pelicula",
        obj: ex
    }));

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let date = req.body.date ? req.body.date : "";
    let memberId = req.body.memberId ? req.body.memberId : false;

    let genre = new Object({
        _date:date,
        _memberId:memberId
    });

    Booking.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => res.status(200).json({
        message: "Objeto reemplazado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo reemplazar el director",
        obj: ex
    }));
}

function update(req, res, next) {
    const id= req.params.id;
    let date = req.body.date;
    let memberId = req.body.memberId;

    let genre = new Object();

    if(date){
        genre._date=date;
    }

    if(memberId){
        genre._memberId=memberId;
    }

    Booking.findOneAndUpdate({"_id":id}, genre,{new : true}).then(obj => res.status(200).json({
        message: "Objeto actualizado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo actualizar el director",
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Booking eliminado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo eliminar el director",
        obj: ex
    }));
}


module.exports = { list, index, create, replace, update, destroy };