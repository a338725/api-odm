const express = require('express');
const Member = require('../models/member');

function list(req, res, next) {
    Member.find().then(objs => res.status(200).json({
        message: "Lista de members",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consutlar la informacion",
        obj:ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Member.findOne({"_id":id}).then(obj => res.status(200).json({
        message:`Member con id ${id}`,
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo encontrar un member con ID ${id}`,
        obj: ex
    }));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const address = new Object();
    address.street = req.body.street;
    address.number = req.body.number;
    address.zip = req.body.zip;
    address.state = req.body.state;

    //console.log(address);
    let member = new Member({
        name: name,
        lastName: lastName,
        phone: phone,
        address: address
    });

    member.save().then(obj => res.status(200).json({
        message:"Member creado correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo almacenar el member",
        obj: ex
    }));
}

function replace(req, res, next) {
    const id= req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : false;

    let genre = new Object({
        _name:name,
        _lastName:lastName
    });

    Member.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => res.status(200).json({
        message: "Objeto reemplazado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo reemplazar el member",
        obj: ex
    }));
}

function update(req, res, next) {
    const id= req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let genre = new Object();

    if(name){
        genre._name=name;
    }

    if(lastName){
        genre._lastName=lastName;
    }

    Member.findOneAndUpdate({"_id":id}, genre,{new : true}).then(obj => res.status(200).json({
        message: "Objeto actualizado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo actualizar el member",
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Member eliminado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo eliminar el member",
        obj: ex
    }));
}


module.exports = { list, index, create, replace, update, destroy };