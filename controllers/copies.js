const express = require('express');
const { format } = require('morgan');
const Copy = require('../models/copy');
const Movie = require('../models/movie');


function list(req, res, next) {
    Copy.find().then(objs => res.status(200).json({
        message: "Lista de copias",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consutlar la informacion",
        obj:ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Copy.findOne({"_id":id}).then(obj => res.status(200).json({
        message:`Copy con id ${id}`,
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo encontrar una copia con ID ${id}`,
        obj: ex
    }));
}


async function create(req, res, next) {
    const number = req.body.number;
    const movieId = req.body.movieId;
    const format = req.body.format;
    const status = req.body.status;

    let movie = await Movie.findOne({"_id": movieId});

    let copy = new Copy({
        number: number,
        format: format,
        movie:movie,
        status:status,
    });

    copy.save().then(obj => res.status(200).json({
        message:"Pelicula creada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo almacenar la pelicula",
        obj: ex
    }));

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let title = req.body.title ? req.body.title : "";
    let movieId = req.body.movieId ? req.body.movieId : false;

    let movie = new Object({
        _title:title,
        _movieId:movieId
    });

    Copy.findOneAndUpdate({"_id":id}, movie, {new : true}).then(obj => res.status(200).json({
        message: "Objeto reemplazado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo reemplazar el director",
        obj: ex
    }));
}

function update(req, res, next) {
    const id= req.params.id;
    let title = req.body.title;
    let movieId = req.body.movieId;

    let movie = new Object();

    if(title){
        movie._title=title;
    }

    if(movieId){
        movie._movieId=movieId;
    }

    Copy.findOneAndUpdate({"_id":id}, movie,{new : true}).then(obj => res.status(200).json({
        message: "Objeto actualizado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo actualizar el director",
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Copy.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Copy eliminado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message:"No se pudo eliminar el director",
        obj: ex
    }));
}


module.exports = { list, index, create, replace, update, destroy };