var express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

function home(req, res, next) {
    res.render('index', { title: 'Express' });
}

function login(req,res,next){
    const email = req.body.email;
    const password = req.body.password;

    User.findOne({"_email":email}).select('_password _salt')
    .then((user)=>{
        if(user){
            bcrypt.hash(password, user.salt, (err, hash)=>{
                if(err){
                    res.status(403).json({
                        message:"No se pudo realizar el login",
                        obj:err
                    });
                }

                if(hash === user.password){
                    const jwtKey = "123456789";
                    res.status(200).json({
                        message:"Sesion iniciada correctamente",
                        obj:jwt.sign({exp: Math.floor(Date.now()/1000)+60}, jwtKey)
                    });
                }else{
                    res.status(403).json({
                        message:"Usuario y/o contraseña incorrectos",
                        obj:null,
                    });
                }
            });

        }else{
            res.status(403).json({
                message:"Usuario y/o contraseña incorrectos",
                obj:null,
            });
        }
    }).catch(err => {
        res.status(403).json({
            message:"No se pudo realizar el login",
            obj:null,
        });
    });
}

module.exports = { home, login };