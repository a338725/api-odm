FROM node
LABEL Dante Rodriguez
WORKDIR /app
COPY . .
ENV HOME video-club
RUN npm install
EXPOSE 3000
CMD npm start


